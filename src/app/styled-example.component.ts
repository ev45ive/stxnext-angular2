import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'stx-styled-example',
  template: `
     <ul class="list-group">
      <li class="list-group-item" (click)=" active = !active " [class.active]="active">   Cras justo odio</li>
      <li class="list-group-item" [style.color]="options.myColor">   Cras justo odio</li>
      <li class="list-group-item" [style.borderBottom]=" getBorder() ">Dapibus ac facilisis in</li>
      <li class="list-group-item" [style.fontSize.px]=" options.fontSize ">Morbi leo risus</li>
      <li class="list-group-item" [class.active]=" ! options.active ">Morbi leo risus</li>
      <li class="list-group-item" [ngClass]=" { 'active': options.active } " >Porta ac consectetur ac</li>
      <li class="list-group-item" [ngStyle]=" { border: '1px solid ' + (options.active? 'red' : 'blue') } ">Vestibulum at eros</li>
    </ul>
  `,
  styles: []
})
export class StyledExampleComponent implements OnInit {

  options = {
    myColor: 'red',
    fontSize: 1,
    active: false
  }

  getBorder(){
    return '3px solid '+ this.options.myColor 
  }

  constructor() {
    // setInterval( () => {
    //   this.options.active = !this.options.active
    //  // this.options.fontSize++;
    // },300);
  }

  ngOnInit() {
  }

}
