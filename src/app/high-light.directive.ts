import { Directive, ElementRef, OnInit, Input, HostListener, HostBinding } from '@angular/core';

@Directive({
  selector: '[stx-highlight]',
  exportAs: 'highlight',
  // host:{
  //   '(mouseenter)':'onMouseEnter($event)'
  //   '[style.borderBottomColor]':'activeColor
  // },
  //inputs:[]
})
export class HighLightDirective implements OnInit{

  constructor( private elem:ElementRef ) {
    //console.log(elem)
  }

  @HostBinding('style.borderBottomColor') get getColor(){
    return !this.active? this.color : '';
  }

  active = false;

  @HostListener('mouseenter',['$event'])
  onMouseEnter(event){
    //this.elem.nativeElement.style.borderBottomColor = 'inherit'
    this.active = true;
  }

  @HostListener('mouseleave',['$event'])
  onMouseLeave(event){
    //this.elem.nativeElement.style.borderBottomColor = this.color
    this.active = false;
  }

  @Input('stx-highlight')
  color  = 'rebeccapurple'

  ngOnInit(){
    //this.elem.nativeElement.style.borderBottomColor = this.color
  }

  // @HostListener('activate',['$event'])
  // onSelect(playlist){
  //   console.log(playlist)
  // }
  
 
}
