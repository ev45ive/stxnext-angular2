import { Component } from '@angular/core';

@Component({
  selector: '.app-root',
  template: `
    <stx-navbar></stx-navbar>
    <div class="container">
      <div class="row">
        <div class="col">
          <router-outlet></router-outlet>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'app works!';

  set value(value){
    this.test = { value }
  }

  test = {
    value: 'test'
  }

  constructor() {

  }
}
