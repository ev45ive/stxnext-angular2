import { Component, OnInit, Input, Inject, ViewChild, AfterViewInit} from '@angular/core';
import { ActivatedRoute } from '@angular/router'
import { PlaylistsService, Playlist } from './playlists.service'
import { NgForm }  from '@angular/forms'

@Component({
  selector: 'stx-playlist-detail',
  template: `

    <div *ngIf="!playlist">
      Proszę wybrac playlistę
    </div>
    <div *ngIf="playlist">
      <form #playlistForm="ngForm" (ngSubmit)="save(playlistForm)" novalidate>
        <div class="input-group">
          <label>Name</label>
          <input type="text" [ngModel]="playlist.name" #name_model="ngModel" 
                required minlength="3" name="name">
          <div *ngIf="name_model.errors?.required"> Wymagane </div>
        </div>
        <!-- <div class="input-group">
          <label>Confim Name</label>
          <input type="text" 
              ngModel name="name_confirm"
              #name_confirm="ngModel" 
              [custom_confirm]="name_model" >
          <div *ngIf="name_confirm.errors?.custom_confirm"> Pole musi byc takie same </div>
        </div>-->
        <div class="input-group">
          <label>Favourite</label>
          <input type="checkbox"  [ngModel]="playlist.favourite" #fav="ngModel" name="favourite">
        </div>
       <div class="input-group">
          <label>Color</label>
          <input type="color" [ngModel]="playlist.color" #color="ngModel" name="color">
      </div>
      <button>Save</button>
      <button>Cancel</button>
      </form>
      </div>
  `,
  styles: []
})
export class PlaylistDetailComponent implements OnInit, AfterViewInit {

  // @Input('playlist') set makeCopy(playlist){
  //   //this.playlist = Object.assign({}, playlist);
  //   this.playlist = playlist;
  // }
  // @ViewChild('playlistForm',{read: NgForm})
  // playlistForm;

  save(form){
    console.log(form)
    let playlist = Object.assign({}, this.playlist, form.value);
    this.service.savePlaylist(playlist)
  }

  playlist:Playlist

  constructor(private service: PlaylistsService,
    private route: ActivatedRoute) {
      //console.log(route.snapshot.data)
 }

  ngOnInit() {
    // this.playlist = this.service
    // .getPlaylist(this.route.snapshot.params['id'])
  }

  ngAfterViewInit(){
    this.route.params
      .map(params =>  params['id'])
      .filter(_ => !!_)
      .flatMap( _ => this.service.getPlaylist$(_) )
      .subscribe(_ => this.playlist = _ )
  }

}
