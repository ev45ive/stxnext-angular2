import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs'
import { Http } from '@angular/http'

export interface Playlist{
  id: number,
  name: string
  color:string,
  favourite: boolean
}

@Injectable()
export class PlaylistsService {

  playlists:Playlist[] = [
    {
      "id": 1,
      "name": "Angular greatest Hits!",
      "color": "#ff0000",
      "favourite": false
    },
    {
      "id": 2,
      "name": "The best of jQuery, vol.3!",
      "color": "#00ff00",
      "favourite": false
    },
    {
      "id": 3,
      "name": "Angular songs collection",
      "color": "#0000ff",
      "favourite": false
    }]

  constructor(private http:Http) {
    
  }
  playlists$ = new Subject<Playlist[]>();

  url = 'http://localhost:3000/playlists/';

  fetchPlaylists(){
       this.http.get(this.url)
      .map( resp => resp.json() )
      .subscribe( playlists => {
        this.playlists = playlists
        this.playlists$.next(playlists)
      })
  }

  getPlaylist$(search_id) {
    return this.http.get(this.url+search_id)
            .map( resp => <Playlist> resp.json() )
  }

  savePlaylist(playlist) {
    if (playlist.id) {
      // save
      this.http.put(this.url + playlist.id, playlist)
        .map( resp => resp.json() )
        .subscribe(playlist => {
            this.fetchPlaylists()
        })
    } else {
      // create
    }
  }

  getPlaylists$() {
    let stream = this.playlists$.startWith(this.playlists)
    this.fetchPlaylists()
    return stream;
  }

}
