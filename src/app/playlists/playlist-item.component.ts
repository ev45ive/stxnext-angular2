import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'stx-playlist-item, .stx-playlist-item',
  template: `
    <p> {{playlist.name}}</p>
    <button class="btn btn-sm" [routerLink]="['show',playlist.id]">Select</button>
  `,
  styles: [],
  //inputs: ['playlist']
})
export class PlaylistItemComponent implements OnInit {

  @Input('data')
  playlist;

  @Output('activate')
  activation = new EventEmitter()

  select(){
    this.activation.emit(this.playlist)
  }

  constructor() { }

  ngOnInit() {
  }

}
