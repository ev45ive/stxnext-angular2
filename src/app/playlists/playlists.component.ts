import { Component, OnInit } from '@angular/core';
import { PlaylistsService } from './playlists.service'

@Component({
  selector: 'stx-playlists',
  template: `<div class="row">

      <div class="col">
        <stx-card title="Playlists">

          <div class="footer"> * pick your playlist! </div>

          <stx-playlists-list [playlists]="playlists$ | async"
            (select)="selected=$event">
          </stx-playlists-list>
        </stx-card>
      </div>

      <div class="col">
          <router-outlet></router-outlet>
      </div>
    </div> `,
  styles: []
})
export class PlaylistsComponent implements OnInit {


  playlists = []

  selected;
  
  playlists$;

  constructor(private service:PlaylistsService) { 
    this.selected = this.playlists[0]
    this.playlists$ = service.getPlaylists$()
  }

  ngOnInit() {
  }

}
