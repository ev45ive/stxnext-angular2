import { Component, OnInit, ViewEncapsulation, Input, ViewChild, ViewChildren, QueryList, Renderer, 
Output, EventEmitter, AfterViewInit, ElementRef } from '@angular/core';
import { PlaylistItemComponent } from './playlist-item.component'
import { PlaylistsService } from './playlists.service'
import { Router } from '@angular/router'

@Component({
  selector: 'stx-playlists-list',
  template: `
    <ul class="list-group">
      <li [stx-highlight]="playlist.color"
        #itemref
        (activate)="select($event)"
        [class.active]="playlist == selected"
        class="list-group-item stx-playlist-item"
        *ngFor="let playlist of playlists; trackBy:$index "
        [data]="playlist"></li>
    </ul>  
    <ng-content></ng-content>
  `,
  styles:[`
    .list-group-item {
      border-bottom: 2px solid black;
    }
  `],
  providers:[],
  viewProviders:[]
})
export class PlaylistsListComponent implements OnInit, AfterViewInit {

  //@ViewChild('itemref',{ read: ElementRef})
  //@ViewChild(PlaylistItemComponent)
  @ViewChildren(PlaylistItemComponent)
  items:QueryList<PlaylistItemComponent>;
 
  @Output('select')
  selection = new EventEmitter();

  @Input()
  playlists = [];

  constructor(private service:PlaylistsService, 
              private router:Router,  
              private elem:ElementRef, 
              private renderer:Renderer) {
    //console.log(renderer.setElementStyle(elem.nativeElement,'border','1px solid red'))
  }

  select(playlist){
    this.selected = playlist;
    this.selection.emit(this.selected)
    //this.router.events.subscribe()
    this.router.navigate(['/playlist/show', playlist.id ], {})
  }

  ngAfterViewInit(){
    // console.log(this.items.changes.subscribe() => {

    // })
  }

  @Input()
  selected;


  ngOnInit() {
  }

}
