import { BrowserModule } from '@angular/platform-browser';
import { NgModule, ModuleWithProviders } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule, RequestOptions, Headers } from '@angular/http';

import { AppComponent } from './app.component';
import { CounterComponent } from './counter.component';
import { PlaylistsComponent } from './playlists/playlists.component';
import { PlaylistsListComponent } from './playlists/playlists-list.component';
import { PlaylistItemComponent } from './playlists/playlist-item.component';
import { PlaylistDetailComponent } from './playlists/playlist-detail.component';
//import { StyledExampleComponent } from './styled-example.component';
import { CardComponent } from './card.component';
import { LifeCycleComponent } from './life-cycle.component';
import { HighLightDirective } from './high-light.directive';
import { UnlessDirective } from './unless.directive';
import { MusicSearchModule } from './music-search/music-search.module';
import { ShortenPipe } from './shorten.pipe';
//import { TestingComponent } from './testing.component'
import { PlaylistsService } from './playlists/playlists.service'

import {
  RouterModule, Route, Routes, CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot,
  Resolve
} from '@angular/router';
import { NavbarComponent } from './navbar/navbar.component';
import { CustomConfirmDirective } from './custom-confirm.directive'

class OnlyAdmins implements CanActivate {
  canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return true
  }
}

class ResolvePlaylist<T> implements Resolve<T>{
  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
    return new Promise(resolve => {
      setTimeout(() => {
        resolve('Resolve!!!!')
      }, 2000)
    })
  }
}

const routing = RouterModule.forRoot([
  { path: '', redirectTo: 'playlist', pathMatch: 'full' /* || prefix */ },
  {
    path: 'playlist', component: PlaylistsComponent, children: [
      { path: '', component: PlaylistDetailComponent },
      {
        path: 'show/:id', component: PlaylistDetailComponent,
        canActivate: [OnlyAdmins],
        // resolve: {
        //   playlist: ResolvePlaylist
        // }
      },
      { path: 'edit/:id', component: PlaylistDetailComponent }
    ]
  },
], {
    enableTracing: false,
    useHash: false
  })

export class myRequestOptions extends RequestOptions {
  constructor() {
    super()
    this.headers = new Headers({ 'My-Super-Token': 'lubieplackimalinowe' })
  }
};

@NgModule({
  declarations: [
    AppComponent,
    CounterComponent,
    PlaylistsComponent,
    PlaylistsListComponent,
    PlaylistItemComponent,
    PlaylistDetailComponent,
    //StyledExampleComponent,
    CardComponent,
    LifeCycleComponent,
    HighLightDirective,
    UnlessDirective,
    NavbarComponent,
    CustomConfirmDirective,
    // ShortenPipe,
    // TestingComponent,
  ],
  entryComponents: [],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    routing,
    MusicSearchModule
  ],
  providers: [
    { provide: 'SpotifyURL', useValue: `https://api.spotify.com/v1/search` },
    PlaylistsService,
    OnlyAdmins,
    ResolvePlaylist,
    { provide: 'playlist', useValue: 'playlistResolve' }
    //{ provide:  RequestOptions, useClass: myRequestOptions }
  ],
  exports: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
