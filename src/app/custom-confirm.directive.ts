import { Directive, Input, AfterViewInit} from '@angular/core';
import { NG_VALIDATORS, Validator, AbstractControl} from '@angular/forms'

export class CustomValidator implements Validator{
  bindInTheDarkness(ngModel){
    this.model = ngModel;
  }

  model;

  validate(control:AbstractControl){
    return control.value !== this.model.value?  {custom_confirm: true }:{}
  }
}

@Directive({
  selector: '[custom_confirm]',
  providers:[
    CustomValidator,
    {
      provide: NG_VALIDATORS, 
      useExisting: CustomValidator,
      multi: true
    }
  ]
})
export class CustomConfirmDirective {

  @Input('custom_confirm')
  custom_confirm

  constructor(private validator:CustomValidator) { 
   
  }

  ngAfterViewInit(){
    console.log('custom_confirm', this.custom_confirm)  
     this.validator.bindInTheDarkness(this.custom_confirm)
  }

}
