import { Directive, Input, TemplateRef, ViewContainerRef, ViewRef } from '@angular/core';

@Directive({
  selector: '[stxUnless]',
})
export class UnlessDirective {

  constructor(private tpl:TemplateRef<any>, 
             private view:ViewContainerRef) {
  }

  cache: ViewRef;

  @Input('stxUnless') set isHidden(hide){
    //this.view.createComponent()
    
    if(hide){
      this.cache = this.view.detach(0)
    }else{
      if(this.cache){
        this.view.insert(this.cache)
      }else{
        this.view.createEmbeddedView(this.tpl,{
          '$implicit':{xxx:1}
        },0)
      }
    }
  }


}
