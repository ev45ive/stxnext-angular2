import { Component, OnInit } from '@angular/core';
import { MusicSearchService } from './music-search/music-search.service'

@Component({
  selector: 'stx-testing',
  template: `
    <p>{{test}}</p>
  `,
  styles: []
})
export class TestingComponent implements OnInit {

  albums = []

  constructor(private musicService: MusicSearchService) {  }

  test = 'Testing'

  ngOnInit() {
   this.musicService.getAlbums('test').subscribe((albums) => {
      this.albums = albums
    })
  }

}
