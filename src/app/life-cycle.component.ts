import { Component, Input, ChangeDetectionStrategy,
  ChangeDetectorRef,
  OnInit, 
  OnChanges, 
  DoCheck, 
  AfterContentInit, 
  AfterContentChecked, 
  AfterViewInit, 
  AfterViewChecked, 
  OnDestroy
} from '@angular/core';

@Component({
  selector: 'stx-life-cycle',
  template: `<p> 
    life-cycle Works!
    {{test.value}}

        <input type="text" [(ngModel)]="test.value">
  </p>  `,
  //changeDetection: ChangeDetectionStrategy.OnPush,
  styles: []
})
export class LifeCycleComponent implements 
  OnInit, 
  OnChanges, 
  DoCheck, 
  AfterContentInit, 
  AfterContentChecked, 
  AfterViewInit, 
  AfterViewChecked, 
  OnDestroy {

  @Input()
  test = {
    value: 'test'
  }

  constructor(private cdr:ChangeDetectorRef) {
    
    cdr.detach();
    
    setInterval(()=>{
      cdr.detectChanges()
      //cdr.markForCheck
    },2000)

    console.log('constructor', arguments)
  }
  ngOnInit(){
    console.log('OnInit', arguments)
  }
  ngOnChanges(){
    console.log('OnChanges', arguments)
  }
  ngDoCheck(){
    console.log('DoCheck', arguments)
  }
  ngAfterContentInit(){
    console.log('AfterContentInit', arguments)
  }
  ngAfterContentChecked(){
    console.log('AfterContentChecked', arguments)
  }
  ngAfterViewInit(){
    console.log('AfterViewInit', arguments)
  }
  ngAfterViewChecked(){
    console.log('AfterViewChecked', arguments)
  }
  ngOnDestroy(){
    console.log('OnDestroy', arguments)
  }
}
