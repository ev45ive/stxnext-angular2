import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'stx-navbar',
  template: `
     <nav class="navbar navbar-toggleable-md navbar-light bg-faded">
       <div class="container">        
        <a class="navbar-brand" routerLink="">Music App</a>
        <div class="navbar-nav">
          <a class="nav-item nav-link" routerLinkActive="active" routerLink="/music">Search</a>
          <a class="nav-item nav-link" routerLinkActive="active" routerLink="/playlist">My Music</a>
        </div>
      </div>
    </nav>
  `,
  styles: []
})
export class NavbarComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
