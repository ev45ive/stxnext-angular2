import { Component, OnInit, Inject } from '@angular/core';
import { Album } from './interfaces'
import { MusicSearchService } from './music-search.service'
import { Subscription } from 'rxjs'
import 'rxjs/add/operator/do'

@Component({
  selector: 'stx-music-albums-list',
  template: `
    <div class="card-deck card-deck-justify">
      <div class="card stx-music-album-item" 
          *ngFor="let album of albums$ | async "
          [album]="album" ></div>
    </div>
  `,
  styles: []
})
export class MusicAlbumsListComponent implements OnInit {

  albums: Album[] = [  ]

  constructor( private musicService:MusicSearchService) {  }

  // sub:Subscription;
  albums$;

  ngOnInit() {
      this.albums$ = this.musicService.getAlbums$()
      .do((albums)=>{
        //console.log(albums)
      })
    //  .subscribe((albums)=>{
    //     this.albums = albums
    //   })
  }

  ngOnDestroy(){
    // this.sub.unsubscribe()
    //window.confirm('are your reeally really sure?')
  }

}
