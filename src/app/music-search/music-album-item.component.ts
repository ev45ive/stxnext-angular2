import { Component, OnInit, Input, Inject } from '@angular/core';
import { Album } from './interfaces'

var i = 0;

@Component({
  selector: '.stx-music-album-item',
  template: `
   <img class="card-img-top img-fluid" 
        [src]="album?.images[0].url">
    <div class="card-img-overlay">
      <h5 class="card-title">{{album?.name | shorten:28  }}</h5>
    </div>
  `,
  styles: [`
    :host{
      flex:  0 0 31%  !important;
      margin-bottom: 0.625rem  !important;
      overflow:hidden;
    }
    
    :host():hover .card-img-overlay{
      top: 100%;
    }
    
    .card-img-overlay{
      background: rgba(0,0,0,0.8);
      top:70%;
      color: #fff;
      font-size: 1em !important;
      transition: .2s top ease-out;
    }
  `],
  providers:[
    // { provide: 'counter', useFactory: ()=>{
    //   i++;
    //   console.log(i)
    // }}
  ]
})
export class MusicAlbumItemComponent implements OnInit {

  @Input()
  album:Album;

  constructor(
    //@Inject('counter') counter
  ) { }

  ngOnInit() {
  }

}
