import { Injectable, Inject, OpaqueToken } from '@angular/core';
import { Album } from './interfaces'
import { Http, Response, URLSearchParams } from '@angular/http'
import { Observable, Subject } from 'rxjs'
import 'rxjs/add/operator/map'
import 'rxjs/add/operator/startWith'


export const searchParamsToken = new OpaqueToken('searchParams')

@Injectable()
export class MusicSearchService {

  albums:Album[] = []
  albums$ = new Subject<Album[]>()

  queries$ = new Subject<string>()

  constructor(private http:Http,
    @Inject('SpotifyURL') private url:string,
    @Inject(searchParamsToken) private getParams) {

    this.queries$
    .do( query => {
      this.lastQuery = query;
      localStorage.setItem('query',query)
    })
    .flatMap(query =>  this._request(query) )
    .subscribe((albums:Album[]) => {
      this.albums = albums
      this.albums$.next(albums)
    })

  }

  lastQuery;

  start(query){
    this.search(query)
  }

  search(query){
    this.queries$.next(query)
  }

  getQueries$(){
    return this.queries$.startWith(this.lastQuery)
  }

  getAlbums$(){
    return this.albums$.startWith(this.albums)
  }

  private _request(query){
    return this.http.get(this.url,{
      search: this.getParams(query)
    })
    .map( (response:Response) =>  response.json() )
    .map( json =>  json.albums.items )
  }


}
