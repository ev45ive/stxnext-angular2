import { NgModule, forwardRef } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MusicSearchComponent } from './music-search.component';
import { MusicSearchFormComponent } from './music-search-form.component';
import { MusicAlbumsListComponent } from './music-albums-list.component';
import { MusicAlbumItemComponent } from './music-album-item.component';
import { MusicSearchService, searchParamsToken } from './music-search.service'
import { Http, Response, URLSearchParams } from '@angular/http'
import { ReactiveFormsModule, FormsModule } from '@angular/forms'
import { ShortenPipe } from '../shorten.pipe'

import { RouterModule, Route, Routes } from '@angular/router'

const routing = RouterModule.forChild([
  { path:'music', component: MusicSearchComponent }
])

export function paramFactory(url) {
  const params = new URLSearchParams()
  params.set('type', 'album')
  params.set('market', 'PL')

  return (query) => {
    params.set('query', query)
    return params;
  }
}

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    routing
  ],
  declarations: [
    MusicSearchComponent,
    MusicSearchFormComponent,
    MusicAlbumsListComponent,
    MusicAlbumItemComponent,
    ShortenPipe
  ],
  exports: [
    MusicSearchComponent
  ],
  providers: [
    //{ provide: AbstractSearchService , useClass: MusicSearchService }
    //{ provide: 'x', useValue: forwardRef()}
    //{ provide: 'x', useValue: forwardRef()}
    { provide: searchParamsToken, useFactory: paramFactory, deps: ['SpotifyURL'] },
    //{ provide: MusicSearchService , useClass: MusicSearchService }
    MusicSearchService,
  ]
})
export class MusicSearchModule { }
