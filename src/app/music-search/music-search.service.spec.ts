import { TestBed, inject } from '@angular/core/testing';
import { MusicSearchService, searchParamsToken } from './music-search.service';
import { Http, Response, URLSearchParams,
   ConnectionBackend, RequestOptions,ResponseOptions, 
   BaseRequestOptions, Request } from '@angular/http'
import { MockBackend, MockConnection } from '@angular/http/testing'

fdescribe('MusicSearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MockBackend,
        BaseRequestOptions,
        {provide:'SpotifyURL', useValue:'http//fake.server.com/'},
        {provide: searchParamsToken, useFactory: ()=>{
          const params = new URLSearchParams()
          return (query) => { params.set('query',query); return params }
        }},
        {provide: Http, useFactory: (backend:ConnectionBackend, options:RequestOptions) => {
            return new Http(backend, options)
        }, deps: [MockBackend, BaseRequestOptions ]},
        MusicSearchService
      ]
    });
  });

  beforeEach( inject([MockBackend],(backend:MockBackend)=>{
    backend.connections.subscribe( (conn:MockConnection) => {
        // TEsting correct request
        conn.request.url.search('banana')

        // Mocking response
        let response = new Response( new ResponseOptions({
          body: {albums: {items: [{id:'banana'}] }},
        }))
        conn.mockRespond(response);

    })
  }))

  it('shoud query for albums', inject([MusicSearchService],(service:MusicSearchService)=>{
    // Testinng response
    service.getAlbums('banana').subscribe((albums)=>{
      expect(albums.length).toEqual(1)
    })
  }))

  it('should ...', inject([MusicSearchService], (service: MusicSearchService) => {
    expect(service).toBeTruthy();
  }));
});
