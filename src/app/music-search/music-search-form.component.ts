import { Component, OnInit } from '@angular/core';
import { MusicSearchService } from './music-search.service'
import { FormGroup, FormControl, FormBuilder, Validators, ValidatorFn, AsyncValidatorFn, AbstractControl } from '@angular/forms';
import { Observable } from 'rxjs'
import 'rxjs/add/operator/filter'
import 'rxjs/add/operator/debounceTime'
import 'rxjs/add/operator/distinctUntilChanged'
import 'rxjs/add/operator/combineLatest'

@Component({
  selector: 'stx-music-search-form',
  template: `
    <form [formGroup]="queryForm" onsubmit="return false;" (submit)="$event.preventDefault()">
      <div class="input-group">
        <input formControlName="query" name="query" type="text" class="form-control" placeholder="Słowa kluczowe" 
        (keyup.enter)="search($event.target.value)">
      </div>
      <div *ngIf="queryForm.pending">Sprawdzam ....</div>
      <!-- <stx-messages field="query"></stx-messages> -->
      <div *ngIf="queryForm.controls.query.touched || queryForm.controls.query.dirty">
        <div *ngIf="queryForm.controls.query.errors?.required"> Pole jest wymagane!</div>
        <div *ngIf="queryForm.controls.query.errors?.minlength">  
        musi miec {{ queryForm.controls.query.errors?.minlength.requiredLength}} znaków! </div>
        <div *ngIf="queryForm.controls.query.errors?.startalpha"> Zacznij od litery!</div>
        <div *ngIf="queryForm.controls.query.errors?.asyncalpha"> Zacznij od litery (async) !</div>
      </div>
    </form>
  `,
  styles: [`
    .form-control.ng-touched.ng-invalid, .form-control.ng-dirty.ng-invalid{
      border:2px solid red;
    }
  `]
})
export class MusicSearchFormComponent implements OnInit {

  constructor(private service: MusicSearchService, private builder: FormBuilder) { }

  queryForm:FormGroup;

  ngOnInit() {
    // this.queryForm = new FormGroup({
    //   'query': new FormControl('batman',[])
    // })
    const startalpha:ValidatorFn = (control:AbstractControl ) =>{
      return /^[a-zA-Z]/.test(control.value)? {} : {'startalpha':true };
    }

    const asyncalpha:AsyncValidatorFn = (control:AbstractControl ) =>{
        return Observable.create( ( observer )=> {

          setTimeout( () => {
            observer.next(  /^[a-zA-Z]/.test(control.value)? {} : { 'startalpha':true } )
            observer.complete();
          },200)
      })
      //return Observable.of({'startalpha':true }).delay(2000)
    }

    this.queryForm = this.builder.group({
      'query': this.builder.control('',[
        Validators.required,
        Validators.minLength(3),
        startalpha
      ],[
        asyncalpha
      ])
    })
    // this.queryForm.addControl('query', query)
    let Query = this.queryForm.controls['query'];

    this.service.getQueries$().subscribe( query => {
      Query.setValue(query)
    })

    Observable.combineLatest(
      Query.statusChanges,
      Query.valueChanges
    )
    .debounceTime(400)
    .filter(d => d[0]=='VALID')
    .map( d => d[1])
    // .subscribe((d) => console.log(d))
     // Query.valueChanges
    .distinctUntilChanged()
    .filter( ({length}) => length >= 3 )
    .subscribe((query)=>{
       //if(this.queryForm.controls['query'].valid){
          this.search(query)
       //}
    });
  }

  search(query){
    //$event.keyCode == 13
    this.service.search(query)
  }
}
