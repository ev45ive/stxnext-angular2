// https://api.spotify.com
    // /v1
    // /search?
    // type=album
    // &market=PL
    // &query=batman

export interface SpotifyEntity{
    id:string
    name:string
}

export interface Album extends SpotifyEntity{
    images: AlbumImage[]
    artists: Artist[]
    uri?:string
}

export interface Artist extends SpotifyEntity{}

export interface AlbumImage{
    url:string
    height: number
    width: number
}