import { Component, OnInit, forwardRef, Input } from '@angular/core';
import { MusicSearchService } from './music-search.service'

@Component({
  selector: 'stx-music-search',
  template: `
    <div class="row">
      <div class="col">
        <stx-music-search-form></stx-music-search-form>
      </div>
    </div>
    <div class="row">
        <stx-music-albums-list></stx-music-albums-list>
    </div>
  `,
  styles: [],
  providers:[
    //{provider: 'parentSer', useExisitng: forwardRef()}
    MusicSearchService
  ]
})
export class MusicSearchComponent implements OnInit {

  constructor(private service:MusicSearchService) { }

  @Input()
  query = 'batman'

  ngOnInit() {
    this.service.start(this.query)
  }

}
