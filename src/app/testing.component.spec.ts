import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser'
import { TestingComponent } from './testing.component';
import { MusicSearchService } from './music-search/music-search.service'
import { Http } from '@angular/http'
import {Observable, Subject} from 'rxjs'
import 'rxjs/observable/of'
import { Album } from './music-search/interfaces'

const musicServiceMock = jasmine.createSpyObj('musicMock',[
  'getAlbums'
])

describe('TestingComponent', () => {
  let component: TestingComponent;
  let fixture: ComponentFixture<TestingComponent>;
  let obs:Subject<any[]>;//Observable.of([]);
  let albums:Album[] = []

  beforeEach(async(() => {
    albums = [
      {id:'fake', name:'Fake Album', artists:[], images: []}
    ]
    obs = new Subject();
    (<jasmine.Spy>musicServiceMock.getAlbums).and.returnValue(obs)

    TestBed.configureTestingModule({
      declarations: [ TestingComponent ],
      providers:[
        { provide: MusicSearchService, useValue: musicServiceMock  }
      ],
      imports:[]
    })
    .compileComponents()
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TestingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should get albums from music service', ()=>{
    expect(component.albums.length).toEqual(0)
    obs.next(albums)
    expect(component.albums.length).toEqual(albums.length)
  })

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should render "test" field value', () => {
    let text = fixture.debugElement.query(By.css('p')).nativeElement.textContent
    expect(text).toEqual(component.test);
  });

  it('should update on "test" field value change', () => {
    component.test = "Testing2"
    //fixture.autoDetectChanges()
    fixture.detectChanges()
    let text = fixture.debugElement.query(By.css('p'))
                      .nativeElement.textContent
    expect(text).toEqual(component.test);
  });

});
