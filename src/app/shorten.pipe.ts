import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'shorten',
  //pure: false
})
export class ShortenPipe implements PipeTransform {

  transform(value: any, toLength = 20 ): any {
    return value.length > toLength? (value.substr(0, toLength) + "...") :  value;
  }

}
